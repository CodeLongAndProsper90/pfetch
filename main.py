import pympris
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("--player", "-p", dest="player", help="select a player", type=int)
parser.add_argument("--verbose")

args = parser.parse_args()
if args.verbose:
    print(args)
players = pympris.available_players()
players = list(players)
if args.verbose:
    print(players)
if args.player is None:
    if len(players) > 1:
        print("Cannot guess what you want, I'm a program, not a mind reader!")
        exit(1)
    args.player = 0

if args.player+1 > len(players):
    print("Player id is not in range")
    print(f"(We have {len(players)} player found)")
    exit(1)
player = pympris.MediaPlayer(players[args.player])
if args.verbose:
    print(player)
properties = {}
metadata = player.player.Metadata
name = player.root.Identity
if args.verbose:
    print(metadata)
properties["{{TITLE}}"] = metadata['xesam:title']
properties["{{ALBUM}}"] = metadata['xesam:album']
properties["{{ARTIST}}"] = ', '.join(metadata['xesam:artist'])
properties["{{PLAYER}}"] = name
process = Popen([name.lower(), '--version'], stdout=PIPE)
process.wait()
stdout, _ = process.communicate()
version = stdout.decode().split('\n')[0].split()[1]
properties["{{VERSION}}"]=version
with open("cmus.art") as art:
    for line in art.readlines():
        for item, value in properties.items():
            line = line.replace(item, value)
        print(line, end='')
